package com.example.kt_telematickotlin.data

import com.example.kt_telematickotlin.database.model.DetailModel
import com.example.kt_telematickotlin.database.model.UserDataModel
import java.util.*

open interface DataManger {
    fun addUsers(username: String?)
    fun retriveDetailsBasedOnUserName(s: String?): ArrayList<DetailModel>?
    fun retriveAllDetails(): ArrayList<UserDataModel>?
    fun addDetails(detailModel: DetailModel?)
}
