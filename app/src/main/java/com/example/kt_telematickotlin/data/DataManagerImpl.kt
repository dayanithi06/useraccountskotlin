package com.example.kt_telematickotlin.data

import com.example.kt_telematickotlin.app.MyApplication
import com.example.kt_telematickotlin.database.model.DetailModel
import com.example.kt_telematickotlin.database.model.UserDataModel
import java.util.*
import javax.inject.Inject

open class DataManagerImpl(application: MyApplication) : DataManger {
    @Inject
 lateinit   var databaseRealm: DatabaseRealm
    private val application: MyApplication
    override fun addUsers(username: String?) {
        databaseRealm!!.addUsers(username)
    }

    override  fun retriveDetailsBasedOnUserName(s: String?): ArrayList<DetailModel> {
        return databaseRealm!!.retriveUserInformationBasedOnUsername(s)
    }

    override  fun retriveAllDetails(): ArrayList<UserDataModel>? {
        return databaseRealm!!.retriveAllUsers()
    }

    override  fun addDetails(detailModel: DetailModel?) {
        databaseRealm!!.addUserInformation(detailModel)
    }

    init {
        this.application = application
        application.appComponent?.inject(this)
    }
}
