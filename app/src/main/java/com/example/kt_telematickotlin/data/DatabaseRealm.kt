package com.example.kt_telematickotlin.data

import android.app.Application
import android.content.Context
import com.example.kt_telematickotlin.app.MyApplication
import com.example.kt_telematickotlin.database.model.DetailModel
import com.example.kt_telematickotlin.database.model.UserDataModel
import io.realm.Realm
import io.realm.RealmConfiguration
import io.realm.RealmResults
import java.util.*

open class DatabaseRealm(application: MyApplication) {
    private var application: MyApplication? = null
    private var mContext: Context? = null
    var realm: Realm? = null//realm = Realm.getInstance(realmConfiguration);

    /**
     * Instance will be thrown
     */
    val realmInstance: Realm?
        get() {
            if (realm != null) {
                return realm
            }
            //realm = Realm.getInstance(realmConfiguration);
            realm = Realm.getDefaultInstance()
            return realm
        }

    /**
     * Intial setup will be intialted
     */
    fun setup() {
        Realm.init(mContext)
        val config =
            RealmConfiguration.Builder().name("default.realm").deleteRealmIfMigrationNeeded()
                .allowQueriesOnUiThread(true).allowWritesOnUiThread(true)
                .build()
        Realm.getInstance(config)
        Realm.setDefaultConfiguration(config)
    }

    fun addUsers(username: String?) {
        val model = UserDataModel()
        model.id=1
        if (username != null) {
            model.username=username
        }
        val realm = realmInstance
        realm!!.executeTransaction { realm -> realm.insertOrUpdate(model) }
    }

    fun retriveUsers(username: String?): UserDataModel {
        return Realm.getDefaultInstance().where(UserDataModel::class.java)
            .contains("username", username).findFirst()!!
    }

    fun retriveAllUsers(): ArrayList<UserDataModel>? {
        val list: ArrayList<UserDataModel>? = ArrayList<UserDataModel>()
        val model: RealmResults<UserDataModel> = Realm.getDefaultInstance().where(
            UserDataModel::class.java
        ).findAll()
        for (model1 in model) {
            list?.add(model1)
        }
        return list
    }

    fun addUserInformation(detailModel: DetailModel?) {
        val realm = realmInstance
        realm!!.executeTransaction { realm -> realm.insertOrUpdate(detailModel) }
    }

    fun retriveUserInformationBasedOnUsername(Username: String?): ArrayList<DetailModel> {
        val list: ArrayList<DetailModel> = ArrayList<DetailModel>()
        val model: RealmResults<DetailModel> = Realm.getDefaultInstance().where(
            DetailModel::class.java
        ).contains("username", Username).findAll()
        for (model1 in model) {
            list.add(model1)
        }
        return list
    }

    init {
        this.application = application
        application.appComponent?.inject(this)
        mContext = application.getApplicationContext()
    }
}

