package com.example.kt_telematickotlin.ui.dialog

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import androidx.fragment.app.DialogFragment
import com.example.kt_telematickotlin.R
import com.example.kt_telematickotlin.app.MyApplication
import com.example.kt_telematickotlin.database.model.DetailModel
import com.example.kt_telematickotlin.viewmodel.MainViewModel

class AddDetailsDialog : DialogFragment() {
    var edittext_name: EditText? = null
    var edittext_area: EditText? = null
    var edittext_city: EditText? = null
    var edittext_state: EditText? = null
    var button_done: Button? = null
    var mainViewModel: MainViewModel? = null
    var userNameFromIntent: String? = null
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val convertview: View =
            inflater.inflate(R.layout.dialog_enter_details, container, false)
        (activity!!.application as MyApplication).appComponent?.inject(mainViewModel)
        userNameFromIntent = arguments!!.getString("username")
        intialize(convertview)
        setListner()
        return convertview
    }

    fun intialize(convertview: View) {
        edittext_name = convertview.findViewById(R.id.edittext_name)
        edittext_area = convertview.findViewById(R.id.edittext_area)
        edittext_city = convertview.findViewById(R.id.edittext_city)
        edittext_state = convertview.findViewById(R.id.edittext_state)
        button_done = convertview.findViewById(R.id.button_done)
    }

    fun setListner() {
        button_done!!.setOnClickListener {
            if (validate()) {
                val detailModel = DetailModel()
                detailModel.username=userNameFromIntent!!
                detailModel.name=(edittext_name!!.text.toString())
                detailModel.area=(edittext_area!!.text.toString())
                detailModel.city=(edittext_city!!.text.toString())
                detailModel.state=(edittext_state!!.text.toString())
                mainViewModel?.rx_user_update?.onNext(detailModel)
                dismiss()
            }
        }
    }

    fun validate(): Boolean {
        return if (edittext_name!!.text.toString().isEmpty() || edittext_area!!.text.toString()
                .isEmpty() || edittext_city!!.text.toString()
                .isEmpty() || edittext_city!!.text.toString().isEmpty()
        ) {
            false
        } else {
            true
        }
    }

    fun setViewModel(model: MainViewModel?) {
        mainViewModel = model
    }
}
