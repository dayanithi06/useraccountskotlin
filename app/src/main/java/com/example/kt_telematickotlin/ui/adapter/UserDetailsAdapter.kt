package com.example.kt_telematickotlin.ui.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.kt_telematickotlin.R
import com.example.kt_telematickotlin.database.model.UserDataModel
import com.example.kt_telematickotlin.viewmodel.MainViewModel
import java.util.*

class UserDetailsAdapter(
    listdata: ArrayList<UserDataModel>,
    context: Context
) :
    RecyclerView.Adapter<UserDetailsAdapter.ViewHolder>() {
    private val context: Context
    private val listdata: ArrayList<UserDataModel>
    var viewModel: MainViewModel? = null
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val listItem: View =
            layoutInflater.inflate(R.layout.item_list_detailsuser, parent, false)
        return ViewHolder(listItem)
    }

    override fun onBindViewHolder(
        holder: ViewHolder,
        position: Int
    ) {
        val myListData: UserDataModel = listdata[position]
        holder.textviewDescription.setText(myListData.username)
        holder.textviewDescription.setOnClickListener { viewModel?.rx_update?.onNext(myListData.username) }
    }

    override fun getItemCount(): Int {
        return listdata.size
    }

  fun init(model: MainViewModel?) {
        viewModel = model
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var textviewDescription: TextView

        init {
            textviewDescription =
                itemView.findViewById<View>(R.id.textviewDescription) as TextView
        }
    }

    // RecyclerView recyclerView;
    init {
        this.listdata = listdata
        this.context = context
    }
}