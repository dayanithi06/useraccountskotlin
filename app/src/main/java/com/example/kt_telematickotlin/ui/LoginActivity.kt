package com.example.kt_telematickotlin.ui

import com.example.kt_telematickotlin.MainActivity
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.kt_telematickotlin.R
import com.example.kt_telematickotlin.app.MyApplication
import com.example.kt_telematickotlin.viewmodel.LoginViewModel
import com.google.android.material.textfield.TextInputEditText

class LoginActivity : AppCompatActivity() {
    var buttonLogin: Button? = null
    var editTextUserName: EditText? = null
    var etPassword: TextInputEditText? = null
    var viewModel: LoginViewModel? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        viewModel = LoginViewModel()
        (application as MyApplication).appComponent?.inject(viewModel)
        initalize()
        setListner()
    }

    private fun setListner() {
        buttonLogin!!.setOnClickListener {
            if (isValidate) {
                viewModel?.login(
                    editTextUserName!!.text.toString(),
                    etPassword!!.text.toString()
                )
                val intent = Intent(this@LoginActivity, MainActivity::class.java)
                intent.putExtra("UserName", editTextUserName!!.text.toString())
                startActivity(intent)
            } else {
                Toast.makeText(
                    this@LoginActivity,
                    "Enter Username/password",
                    Toast.LENGTH_SHORT
                ).show()
            }
        }
    }

    private val isValidate: Boolean
        private get() = if (editTextUserName!!.text.toString().length > 0 && etPassword!!.text
                .toString().length > 0
        ) {
            true
        } else {
            false
        }

    private fun initalize() {
        buttonLogin = findViewById<View>(R.id.buttonLogin) as Button
        editTextUserName = findViewById<View>(R.id.editTextUserName) as EditText
        etPassword = findViewById<View>(R.id.etPassword) as TextInputEditText
    }
}