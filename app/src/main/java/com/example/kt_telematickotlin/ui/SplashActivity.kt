package com.example.kt_telematickotlin.ui

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import com.example.kt_telematickotlin.R
import com.example.kt_telematickotlin.useraccounts.TAccountActivity

class SplashActivity : Activity() {
    var handler: Handler? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        handler = Handler()
        handler!!.postDelayed({
            val intent = Intent(this@SplashActivity, TAccountActivity::class.java)
            startActivity(intent)
            finish()
        }, 5000)
    }
}