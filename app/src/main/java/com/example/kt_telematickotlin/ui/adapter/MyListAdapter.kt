package com.example.kt_telematickotlin.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.kt_telematickotlin.R
import com.example.kt_telematickotlin.database.model.DetailModel
import java.util.*

class MyListAdapter(listdata: ArrayList<DetailModel>?) :
    RecyclerView.Adapter<MyListAdapter.ViewHolder>() {
    private val listdata: ArrayList<DetailModel>?
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val listItem: View =
            layoutInflater.inflate(R.layout.item_detail_list, parent, false)
        return ViewHolder(listItem)
    }

    override fun onBindViewHolder(
        holder: ViewHolder,
        position: Int
    ) {
        var myListData: DetailModel = listdata?.get(position) !!
        holder.textviewDescription.setText(
            myListData?.name+("\n")+(myListData.username)
        )
    }

    override fun getItemCount(): Int {
        return listdata!!.size
    }

    fun clearItems() {
        val size = listdata?.size
        listdata?.clear()
        notifyDataSetChanged()
        this.notifyItemRangeChanged(0, size!!)
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var textviewDescription: TextView

        init {
            textviewDescription =
                itemView.findViewById<View>(R.id.textviewDescription) as TextView
        }
    }

    // RecyclerView recyclerView;
    init {
        this.listdata = listdata
    }
}