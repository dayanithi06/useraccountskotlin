package com.example.kt_telematickotlin.ui.dialog

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.Toast
import androidx.fragment.app.DialogFragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.kt_telematickotlin.R
import com.example.kt_telematickotlin.app.MyApplication
import com.example.kt_telematickotlin.database.model.UserDataModel
import com.example.kt_telematickotlin.ui.adapter.UserDetailsAdapter
import com.example.kt_telematickotlin.useraccounts.AccountUtils
import com.example.kt_telematickotlin.viewmodel.MainViewModel
import java.util.*

class LoginUserAccountsDialog : DialogFragment() {
    var recycerview_accounts: RecyclerView? = null
    var linearlayout_addaccount: LinearLayout? = null
    var usernameFromIntent: String? = null
    var model: MainViewModel? = null
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val convertview: View =
            inflater.inflate(R.layout.dialog_login_useraccounts, container, false)
        (activity!!.application as MyApplication).appComponent?.inject(model)
        usernameFromIntent = arguments!!.getString("username")
        intialize(convertview)
        setAdapter()
        setListner()
        /*    model.rx_update.subscribe(new Observer<String>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(String s) {
                dismiss();
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        });*/return convertview
    }

    private fun setListner() {
        linearlayout_addaccount!!.setOnClickListener {
            Toast.makeText(
                activity,
                "Please use login page",
                Toast.LENGTH_SHORT
            ).show()
        }
        linearlayout_addaccount!!.visibility = View.GONE
    }

    fun intialize(convertView: View) {
        recycerview_accounts = convertView.findViewById(R.id.recycerview_accounts)
        linearlayout_addaccount =
            convertView.findViewById(R.id.linearlayout_addaccount)
    }

    fun setAdapter() {

        //  ArrayList<UserDataModel> detailModelArrayList = model.dataManger.retriveAllDetails();
        val detailModelArrayList: ArrayList<UserDataModel> =
            AccountUtils.getAllAccounts(activity)
        val adapter = activity?.let { UserDetailsAdapter(detailModelArrayList, it) }
        adapter?.init(model)
        recycerview_accounts!!.layoutManager = LinearLayoutManager(activity)
        recycerview_accounts!!.adapter = adapter
    }

    fun setViewModel(model: MainViewModel?) {
        this.model = model
    }
}
