package com.example.kt_telematickotlin.app

import android.app.Application
import android.content.Context
import com.example.kt_telematickotlin.dagger.AppComponent
import com.example.kt_telematickotlin.dagger.AppModule
import com.example.kt_telematickotlin.dagger.DaggerAppComponent
import com.example.kt_telematickotlin.data.DatabaseRealm
import javax.inject.Inject

class MyApplication : Application() {
    var appComponent: AppComponent? = null
    var context: Context? = null

    @Inject
    lateinit var realm: DatabaseRealm
    override fun onCreate() {
        super.onCreate()
        appComponent = DaggerAppComponent.builder()
            .appModule(AppModule(this))
            .build()
        appComponent?.inject(this)
        initRealm()
        getContext()
    }

   /* fun getAppComponent(): AppComponent? {
        return appComponent
    }*/

    fun getContext() {
        context = applicationContext
    }

    fun initRealm() {
        realm?.setup()
    }
}