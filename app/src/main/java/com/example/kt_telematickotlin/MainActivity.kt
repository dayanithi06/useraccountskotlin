package com.example.kt_telematickotlin

import android.content.Context
import android.content.SharedPreferences
import android.content.SharedPreferences.Editor
import android.os.Bundle
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.kt_telematickotlin.app.MyApplication
import com.example.kt_telematickotlin.database.model.DetailModel
import com.example.kt_telematickotlin.ui.adapter.MyListAdapter
import com.example.kt_telematickotlin.ui.dialog.AddDetailsDialog
import com.example.kt_telematickotlin.ui.dialog.SwitchAccountDialog
import com.example.kt_telematickotlin.viewmodel.MainViewModel
import io.reactivex.Observer
import io.reactivex.disposables.Disposable
import java.util.*

open class MainActivity : AppCompatActivity() {
    var imagebg: ImageView? = null
    var recyclerView: RecyclerView? = null
    var UserNameFromIntent: String? = null
    var model: MainViewModel? = null
    var button_adddetail: Button? = null
    var textviewNameintial: TextView? = null
    var detailsDialog: AddDetailsDialog? = null
    var dialog: SwitchAccountDialog? = null
    var adapter: MyListAdapter? = null
    var sharedPreferences: SharedPreferences? = null
    var myEdit: Editor? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        model = MainViewModel()
        (application as MyApplication).appComponent?.inject(model)
        setContentView(R.layout.activity_main)
        UserNameFromIntent = intent.getStringExtra("UserName")
        sharedPreferences =
            getSharedPreferences("MySharedPref", Context.MODE_PRIVATE)
        myEdit = sharedPreferences?.edit()
        intialize()
        setAdapter()
        setListner()
        registerobs()
        textviewNameintial!!.text = UserNameFromIntent?.substring(0, 1)
        model?.rx_user_update?.subscribe(object : Observer<DetailModel?> {
            override fun onSubscribe(d: Disposable) {}
            override fun onNext(detailModel: DetailModel) {
                model?.dataManger?.addDetails(detailModel)
                setAdapter()
            }

            override fun onError(e: Throwable) {
                e.printStackTrace()
            }

            override fun onComplete() {}
        })
        model?.rx_update?.subscribe(object : Observer<String?> {
            override fun onSubscribe(d: Disposable) {}
            override fun onNext(s: String) {
                UserNameFromIntent = s
                setAdapter()
                textviewNameintial!!.text = UserNameFromIntent!!.substring(0, 1)
                dialog?.dismiss()
                myEdit?.putString("UserName", UserNameFromIntent)
                myEdit?.commit()
            }

            override fun onError(e: Throwable) {}
            override fun onComplete() {}
        })
    }

    private fun setListner() {
        imagebg!!.setOnClickListener {
            dialog = SwitchAccountDialog()
            dialog?.setViewModel(model)
            val bundle = Bundle()
            bundle.putString("username", UserNameFromIntent)
            dialog?.setArguments(bundle)
            dialog?.show(supportFragmentManager, "acc")
        }
        button_adddetail!!.setOnClickListener {
            detailsDialog = AddDetailsDialog()
            detailsDialog?.setViewModel(model)
            val bundle = Bundle()
            bundle.putString("username", UserNameFromIntent)
            detailsDialog?.setArguments(bundle)
            detailsDialog?.show(supportFragmentManager, "aa")
        }
    }

    private fun setAdapter() {
        val detailModelArrayList: ArrayList<DetailModel>? =
            model?.dataManger?.retriveDetailsBasedOnUserName(UserNameFromIntent)
        if (detailModelArrayList == null || detailModelArrayList.isEmpty()) {
            if (adapter != null) {
                adapter?.clearItems()
                adapter?.notifyDataSetChanged()
                recyclerView!!.invalidate()
            }
            return
        }
        adapter = MyListAdapter(detailModelArrayList)
        recyclerView!!.layoutManager = LinearLayoutManager(this)
        recyclerView!!.adapter = adapter
        adapter?.notifyDataSetChanged()
        recyclerView!!.invalidate()
    }

    private fun intialize() {
        recyclerView = findViewById(R.id.recyclerView_details)
        imagebg = findViewById(R.id.imagebg)
        button_adddetail = findViewById(R.id.button_adddetail)
        textviewNameintial = findViewById(R.id.textviewNameintial)
    }

    fun registerobs() {}
}