package com.example.kt_telematickotlin.database.model

import io.realm.RealmObject

open class DetailModel : RealmObject() {

    var id = 0
    var username = ""
    var name = ""
    var area = ""
    var city = ""
    var state = ""
}
