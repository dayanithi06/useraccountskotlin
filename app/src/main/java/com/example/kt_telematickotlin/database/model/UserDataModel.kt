package com.example.kt_telematickotlin.database.model

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

open class UserDataModel : RealmObject() {

    var id = 0

    @PrimaryKey
    var username = ""
}
