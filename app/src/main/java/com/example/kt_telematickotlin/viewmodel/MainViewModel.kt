package com.example.kt_telematickotlin.viewmodel

import android.os.Bundle
import com.example.kt_telematickotlin.MainActivity
import com.example.kt_telematickotlin.app.MyApplication
import com.example.kt_telematickotlin.data.DataManger
import com.example.kt_telematickotlin.database.model.DetailModel
import io.reactivex.subjects.BehaviorSubject
import javax.inject.Inject

class MainViewModel : MainActivity() {
    var rx_update: BehaviorSubject<String> = BehaviorSubject.create()
    var rx_user_update: BehaviorSubject<DetailModel> = BehaviorSubject.create()

    @Inject
  lateinit  var dataManger: DataManger
    protected override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        (getApplication() as MyApplication).appComponent?.inject(this)
    }
}