package com.example.kt_telematickotlin.viewmodel

import android.app.Activity
import android.os.Bundle
import com.example.kt_telematickotlin.app.MyApplication
import com.example.kt_telematickotlin.data.DataManger
import io.reactivex.subjects.BehaviorSubject
import javax.inject.Inject

class LoginViewModel : Activity() {
    var rx_update: BehaviorSubject<String> = BehaviorSubject.create()

    @Inject
    lateinit var dataManger: DataManger
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        (application as MyApplication).appComponent?.inject(this)
    }

    fun login(username: String?, password: String?) {
        dataManger?.addUsers(username)
    }
}