package com.example.kt_telematickotlin.useraccounts

import android.accounts.Account
import android.accounts.AccountManager
import android.content.Context
import com.example.kt_telematickotlin.database.model.UserDataModel
import java.util.*

object AccountUtils {
    const val ACCOUNT_TYPE = "com.tele.example"
    const val AUTH_TOKEN_TYPE = "com.tele.example.aaa"

    //public static IServerAuthenticator mServerAuthenticator = new MyServerAuthenticator();
    fun getAccount(
        context: Context?,
        accountName: String?
    ): Account? {
        var accountManager = AccountManager.get(context)
        var accounts =
            accountManager.getAccountsByType(ACCOUNT_TYPE)
        for (account in accounts) {
            if (account.name.equals(accountName, ignoreCase = true)) {
                return account
            }
        }
        return null
    }

    fun checkPasswordIsCorrect(
        context: Context?,
        accountName: String?,
        password: String
    ): Account? {
        var accountManager = AccountManager.get(context)
        var accounts =
            accountManager.getAccountsByType(ACCOUNT_TYPE)
        for (account in accounts) {
            if (account.name.equals(accountName, ignoreCase = true)) {
                if (password == accountManager.getPassword(account)) return account
            }
        }
        return null
    }

    fun getAllAccounts(context: Context?): ArrayList<UserDataModel> {
        val dataModels: ArrayList<UserDataModel> = ArrayList<UserDataModel>()
        val accountManager = AccountManager.get(context)
        val accounts =
            accountManager.getAccountsByType(ACCOUNT_TYPE)
        for (account in accounts) {
            val dataModel = UserDataModel()
            dataModel.username=(account.name)
            dataModels.add(dataModel)
        }
        return dataModels
    }
}