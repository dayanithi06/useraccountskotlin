package com.example.kt_telematickotlin.useraccounts

import com.example.kt_telematickotlin.MainActivity
import android.accounts.Account
import android.accounts.AccountManager
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.SharedPreferences.Editor
import android.os.AsyncTask
import android.os.Bundle
import android.view.View
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.kt_telematickotlin.R
import com.example.kt_telematickotlin.ui.dialog.LoginUserAccountsDialog
import com.example.kt_telematickotlin.viewmodel.MainViewModel
import io.reactivex.Observer
import io.reactivex.disposables.Disposable
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

class TAccountActivity : AppCompatActivity() {
    private var accountManager: AccountManager? = null
    private val REQ_REGISTER = 11
    var userId: String? = null
    var loginViewModel: MainViewModel? = null
    var dialog: LoginUserAccountsDialog? = null
    var sharedPreferences: SharedPreferences? = null
    var myEdit: Editor? = null
    override fun onCreate(icicle: Bundle?) {
        super.onCreate(icicle)
        setContentView(R.layout.activity_login1)
        sharedPreferences =
            getSharedPreferences("MySharedPref", Context.MODE_PRIVATE)
        myEdit = sharedPreferences?.edit()
        loginViewModel = MainViewModel()
        accountManager = AccountManager.get(baseContext)
        if (intent.getBooleanExtra("addednewUser", false)) {
            //will create new user.
        } else {
            val username = sharedPreferences?.getString("UserName", "")
            if (!username!!.isEmpty()) {
                val intent1 = Intent(this@TAccountActivity, MainActivity::class.java)
                intent1.putExtra("UserName", username)
                intent1.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
                startActivity(intent1)
            } else {
                if (!AccountUtils.getAllAccounts(this).isEmpty()) {
                    dialog = LoginUserAccountsDialog()
                    dialog?.setViewModel(loginViewModel)
                    val bundle = Bundle()
                    bundle.putString("username", username)
                    dialog?.setArguments(bundle)
                    dialog?.show(supportFragmentManager, "acc")
                }
            }
        }
        loginViewModel?.rx_update?.subscribe(object : Observer<String> {
            override fun onSubscribe(d: Disposable) {}
            override fun onNext(s: String) {
                dialog?.dismiss()
                myEdit?.putString("UserName", s)
                myEdit?.commit()
                val intent1 = Intent(this@TAccountActivity, MainActivity::class.java)
                intent1.putExtra("UserName", s)
                intent1.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
                startActivity(intent1)
            }

            override fun onError(e: Throwable) {}
            override fun onComplete() {}
        })
    }

    fun createAccount(view: View?) {
        val intent = Intent(baseContext, TCreateAccountActivity::class.java)
        // intent.putExtras(getIntent().getExtras());
        startActivityForResult(intent, REQ_REGISTER)
    }

    fun login(view: View?) {
        val userId =
            (findViewById<View>(R.id.user) as EditText).text.toString()
        val passWd =
            (findViewById<View>(R.id.password) as EditText).text.toString()
        val accountType = intent.getStringExtra(AccountManager.KEY_ACCOUNT_TYPE)
        object : AsyncTask<Void?, Void?, Intent?>() {
            protected override fun doInBackground(vararg params: Void?): Intent? {
                val data = Bundle()
                val df: DateFormat = SimpleDateFormat("yyyyMMdd-HHmmss")
                val loginAccount =
                    AccountUtils.checkPasswordIsCorrect(this@TAccountActivity, userId, passWd)
                        ?: return null
                val authToken = userId + "-" + df.format(Date())
                // String authToken = ZoftinoAccountRegLoginHelper.authenticate(userId, passWd);
                //  String tokenType = ZoftinoAccountRegLoginHelper.getTokenType(userId);
                data.putString(AccountManager.KEY_ACCOUNT_NAME, userId)
                data.putString(AccountManager.KEY_ACCOUNT_TYPE, accountType)
                data.putString(TAccountAuthenticator.TOKEN_TYPE, TAccountAuthenticator.TOKEN_TYPE)
                data.putString(AccountManager.KEY_AUTHTOKEN, authToken)
                data.putString(TAccountAuthenticator.PASSWORD, passWd)
                val result = Intent()
                result.putExtras(data)
                return result
            }

            override fun onPostExecute(intent: Intent?) {
                setLoginResult(intent)
            }
        }.execute()
    }

    private fun setLoginResult(intent: Intent?) {
        if (intent == null) {
            Toast.makeText(
                this@TAccountActivity,
                "Invalid username/password. Please create a user.",
                Toast.LENGTH_SHORT
            ).show()
            return
        }
        userId = intent.getStringExtra(AccountManager.KEY_ACCOUNT_NAME)
        val passWd = intent.getStringExtra(TAccountAuthenticator.PASSWORD)
        val account =
            Account(userId, AccountUtils.ACCOUNT_TYPE)
        if (intent.extras!!.getBoolean(TAccountAuthenticator.ADD_ACCOUNT, false) == true) {
            val authtoken = intent.getStringExtra(AccountManager.KEY_AUTHTOKEN)
            val tokenType = intent.getStringExtra(TAccountAuthenticator.TOKEN_TYPE)
            accountManager!!.addAccountExplicitly(account, passWd, null)
            accountManager!!.setAuthToken(account, tokenType, authtoken)
        } else {
            myEdit!!.putString("UserName", account.name)
            myEdit!!.putString("password", account.type)
            myEdit!!.commit()
            accountManager!!.setPassword(account, passWd)
            val intent1 = Intent(this@TAccountActivity, MainActivity::class.java)
            intent1.putExtra("UserName", userId)
            intent1.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent1)
        }

        //setAccountAuthenticatorResult(intent.getExtras());
        // setResult(RESULT_OK, intent);
//
        // finish();
    }

    override fun onActivityResult(
        requestCode: Int,
        resultCode: Int,
        data: Intent?
    ) {
        if (resultCode == Activity.RESULT_OK && requestCode == REQ_REGISTER) {
            setLoginResult(data)
        } else {
            // viewModel.login(editTextUserName.getText().toString(), etPassword.getText().toString());
            val intent = Intent(this@TAccountActivity, MainActivity::class.java)
            intent.putExtra("UserName", userId)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
        }
        super.onActivityResult(requestCode, resultCode, data)
    }
}