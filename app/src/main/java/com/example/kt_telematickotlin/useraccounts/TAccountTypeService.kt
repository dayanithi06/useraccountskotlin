package com.example.kt_telematickotlin.useraccounts

import android.app.Service
import android.content.Intent
import android.os.IBinder

class TAccountTypeService : Service() {
    override fun onBind(intent: Intent): IBinder? {
        val authenticator = TAccountAuthenticator(this)
        return authenticator.iBinder
    }
}