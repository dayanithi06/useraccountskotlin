package com.example.kt_telematickotlin.useraccounts

import android.accounts.AccountManager
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.example.kt_telematickotlin.R

class TCreateAccountActivity : AppCompatActivity() {
    private var accountType: String? = null
    var mAccountManager: AccountManager? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
        mAccountManager = AccountManager.get(this)
        accountType = AccountUtils.ACCOUNT_TYPE
    }

    fun createAccount(view: View?) {
        val userId =
            (findViewById<View>(R.id.user) as EditText).text.toString()
        val passWd =
            (findViewById<View>(R.id.password) as EditText).text.toString()
        val name =
            (findViewById<View>(R.id.name) as EditText).text.toString()

        //  if(!ZoftinoAccountRegLoginHelper.validateAccountInfo(name,userId, passWd)){
        //      ((TextView)findViewById(R.id.error)).setText("Please Enter Valid Information");
        //  }

        // String authToken = ZoftinoAccountRegLoginHelper.createAccount(name,userId, passWd);
        //Account account = new Account(userId, accountType);
        val authToken = name + userId + passWd
        // mAccountManager.addAccountExplicitly(account, passWd, null);
        // mAccountManager.setAuthToken(account, accountType, authToken);
        // String authTokenType = ZoftinoAccountRegLoginHelper.getTokenType(userId);
        val authTokenType = TAccountAuthenticator.TOKEN_TYPE
        if (authToken.isEmpty()) {
            (findViewById<View>(R.id.error) as TextView).text =
                "Account couldn't be registered, please try again."
        }
        val data = Bundle()
        data.putBoolean(TAccountAuthenticator.ADD_ACCOUNT, true)
        data.putString(AccountManager.KEY_ACCOUNT_NAME, userId)
        data.putString(AccountManager.KEY_ACCOUNT_TYPE, accountType)
        data.putString(AccountManager.KEY_AUTHTOKEN, authToken)
        data.putString(TAccountAuthenticator.PASSWORD, passWd)
        data.putString(TAccountAuthenticator.TOKEN_TYPE, authTokenType)
        val result = Intent()
        result.putExtras(data)
        setResult(Activity.RESULT_OK, result)
        finish()
    }
}