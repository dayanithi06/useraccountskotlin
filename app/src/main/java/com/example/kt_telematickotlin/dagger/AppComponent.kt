package com.example.kt_telematickotlin.dagger

import com.example.kt_telematickotlin.MainActivity
import com.example.kt_telematickotlin.app.MyApplication
import com.example.kt_telematickotlin.data.DataManagerImpl
import com.example.kt_telematickotlin.data.DatabaseRealm
import com.example.kt_telematickotlin.ui.LoginActivity
import com.example.kt_telematickotlin.viewmodel.LoginViewModel
import com.example.kt_telematickotlin.viewmodel.MainViewModel
import dagger.Component
import javax.inject.Singleton


@Singleton
@Component(modules = [AppModule::class])
interface AppComponent {
    fun inject(application: MyApplication?)
    fun inject(databaseRealm: DatabaseRealm?)
    fun inject(dataManager: DataManagerImpl?)
    fun inject(mainActivity: MainActivity?)
    fun inject(mainViewModel: MainViewModel?)
    fun inject(loginActivity: LoginActivity?)
    fun inject(loginViewModel: LoginViewModel?)
}

