package com.example.kt_telematickotlin.dagger

import android.content.Context
import com.example.kt_telematickotlin.app.MyApplication
import com.example.kt_telematickotlin.data.DataManagerImpl
import com.example.kt_telematickotlin.data.DataManger
import com.example.kt_telematickotlin.data.DatabaseRealm
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule(mApplication: MyApplication) {
    private val mApplication: MyApplication = mApplication

    @Provides
    @Singleton
    fun provideApplication(): MyApplication {
        return mApplication
    }

    @Provides
    @Singleton
    fun provideDatabaseRealm(): DatabaseRealm {
        return DatabaseRealm(mApplication)
    }

    @Provides
    @Singleton
    fun applicationContext(): Context {
        return mApplication.getApplicationContext()
    }

    @Provides
    @Singleton
    fun providesDataManager(): DataManger {
        return DataManagerImpl(mApplication)
    }


}