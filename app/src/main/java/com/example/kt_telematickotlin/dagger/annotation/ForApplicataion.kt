package com.example.kt_telematickotlin.dagger.annotation

import javax.inject.Qualifier
import kotlin.annotation.Retention

@Qualifier
@Retention(AnnotationRetention.RUNTIME)
annotation class ForApplication
